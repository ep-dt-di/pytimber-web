from django import template

register = template.Library()

@register.filter
def lookup(value, key):
    return value.get(key, [])

@register.filter
def fetch_value(value, key):
    return value[key]['value']

@register.filter
def fetch_unit(value, key):
    return value[key]['unit']

# @register.filter
# def fetch_format(value, key):
#     return value[key]['format']
#
# @register.filter
# def fetch_sig_fig(value, key):
#     return value[key]['sig_fig']
#
# @register.filter
# def fetch_lower_limit(value, key):
#     return value[key]['lower_limit']
#
# @register.filter
# def fetch_upper_limit(value, key):
#     return value[key]['upper_limit']

@register.filter
def fetch_group(value, key):
    return value[key]['group']

@register.filter
def index(List, i):
    return List[int(i)]

@register.filter
def split_trunc(value, key):
    # split value by key, return first section only
    return value.split(key)[0]
