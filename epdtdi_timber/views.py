import csv, json, numpy
import datetime, pytz
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, StreamingHttpResponse
from epdtdi_timber.utils import userAuthorised, getToday, getUTC, getGroupedVars, getData, getDipVals, getStructDipVals
from django.conf import settings

class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value

def index(request):
    apps_available = settings.APPS_CONFIGURED.keys()
    app_error = request.GET.get('app_error', '')
    group_error = request.GET.get('group_error', '')
    context = {
        "apps_available": sorted(apps_available),
        "app_error": app_error,
        "group_error": group_error,
    }
    return render(request, 'epdtdi_timber/index.html', context)

def plot_index(request, app_vars=settings.DEFAULT_APP_NAME):
    """An index view that first offers the user the ability to request data."""
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    # set timestamps for today
    fromTS, toTS = getToday()
    # request the app's variables grouped into categories
    groupedVars = getGroupedVars(app_vars)
    # define selected variables
    selectedVariables=[]
    # set the context dictionary for Django's view
    context = {
        "varList": groupedVars,
        "selectedVars": selectedVariables,
        "days": 1,
        "fromUTC": 0,
        "toUTC": 0,
        "fromDTPValue": fromTS,
        "toDTPValue": toTS,
        "what": None,
        'app_vars': app_vars
    }
    # render the request in a view, using context
    return render(request, 'epdtdi_timber/plots.html', context)

def hello(request, app_vars=settings.DEFAULT_APP_NAME, name=""):
    """A test-view that simply renders data from the request URL."""
    # render the hello test view with the user's name
    return render(request, 'epdtdi_timber/hello.html', {
        'name': name,
        'app_vars': app_vars
    })

def trPlot(request, app_vars=settings.DEFAULT_APP_NAME):
    """A view that returns two plots to the user, mapping the requested data
       visually onto 1 matplotlib PNG plot, 1 ChartJS interactive HTML5 canvas
       plot and 1 CanvasJS interactive HTML5 div plot. The ChartJS & CanvasJS
       plots have to be drawn client-side so the data is simply
       provided to the browser via the relevant Django template."""
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    # request the app's variables grouped into categories
    groupedVars = getGroupedVars(app_vars)
    # collect the requested vars from the POST request as a list ~may be empty~
    vars = request.POST.getlist('vars')
    static_vars= request.POST.getlist('vars')
    # collect from datetime from POST request, if empty will be False
    fromDTP = request.POST.get('fromDTP', False)
    # collect to datetime from POST request, if empty will be False
    toDTP = request.POST.get('toDTP', False)
    # get timezone
    timezone = request.POST.get('timezone', False)
    # should either datetime not be defined redirect to index
    if not fromDTP or not toDTP:
        return redirect('index')
    # convert datetimes to UTC format
    fromUTCsecs, toUTCsecs = getUTC(fromDTP, toDTP, timezone)
    # call util function to generate the matplotlib png and return its path
    # along with the data collected and the request elapsed time
    # makeTrPlot used before for static plot
    # plot_path, data, elapsed_time, vars_units, vars_types = makeTrPlot(fromUTCsecs, toUTCsecs, vars, app_vars) 

    data, elapsed_time, vars_units, vars_types = getData(fromUTCsecs, toUTCsecs, vars, app_vars)
    # loop through returned data and convert numpy ndarrays to python standard
    # arrays - since they meet the JSON standard
    for var in vars:
        data[var] = (data[var][0].tolist(), data[var][1].tolist())
    # collect units for each variable in use into a single array
    units = [vars_units[x] for x in vars]
    # collect types for each variable in use into a single array
    types = [vars_types[y] for y in vars]
    # convert sanitized data to JSON format for JS to interpret client-side
    data = json.dumps(data)
    # set the context dictionary for Django's view
    context = {
        "varList": groupedVars,
        "selectedVars": static_vars,
        "dataVars": vars,
        "days": 1,
        "fromUTC": 0,
        "toUTC": 0,
        "fromDTPValue": fromDTP,
        "toDTPValue": toDTP,
        "what": None,
        "data": data,
        "units": units,
        "types": types,
        'app_vars': app_vars
    }
    # render the request in a view, using context
    response = render(request, 'epdtdi_timber/plots.html', context)
    # set cookie on client confirming elapsed DB request time
    response.set_cookie("dataRequestTime", value=elapsed_time)
    # return the crafter response
    return response


def trStreamingFile(request, app_vars=settings.DEFAULT_APP_NAME):
    """A 'view' that streams a large CSV file of the requested data."""
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object
    # collect the requested vars from the POST request as a list ~may be empty~
    vars = request.POST.getlist('vars')
    # collect from datetime from POST request, if empty will be False
    fromDTP = request.POST.get('fromDTP', False)
    # collect to datetime from POST request, if empty will be False
    toDTP = request.POST.get('toDTP', False)
    # should either datetime not be defined redirect to index
    if not fromDTP or not toDTP:
        return redirect('index')

    timezone = request.POST.get('timezone', False)
    # convert datetimes to UTC format
    fromUTCsecs, toUTCsecs = getUTC(fromDTP, toDTP, timezone)
    # if no vars provided no CSV can be generated, so send fail response
    if vars is None or len(vars) < 1:
        response = HttpResponseBadRequest("Failed to provide variables.")
        response.set_cookie("fileFailed", value="False")
        return response
    # call util function to request required data from the DB and collect
    # the DB request elapsed time
    data, elapsed_time, vars_units, vars_types = getData(fromUTCsecs, toUTCsecs, vars, app_vars)
    # if no data returned, fail gracefully for user and inform browser
    if data is None:
        response = HttpResponseBadRequest("No data available.")
        response.set_cookie("fileFailed", value="False")
        return response

    # start creating rows for csv
    temp = []
    for key, val in data.items():
        temp = temp + list(zip(data[key][0],data[key][1],[key]*len(data[key][0])))
    temp.sort(key=lambda tup: tup[0])

    rows = []
    for item in temp:
        date1 = datetime.datetime.fromtimestamp(item[0], pytz.utc)
        date1 = date1.astimezone(pytz.timezone(timezone))
        if len(rows)!=0 and rows[len(rows)-1]['Datetime'] == date1.strftime('%d/%m/%Y %H:%M:%S'):
            row[item[2]] = item[1]
        else:
            row = {'Datetime': date1.strftime('%d/%m/%Y %H:%M:%S')}
            for var in vars:
                row[var] = ''

            row[item[2]] = item[1]

            rows.append(row)

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="pytimber' + datetime.datetime.now().strftime("%d_%m_%Y_%H_%M_%S") + '.csv"'
    response.set_cookie("fileDownloaded", value=elapsed_time)
    fieldnames = ['Datetime']+vars
    writer = csv.DictWriter(response, fieldnames=fieldnames)

    writer.writeheader()
    for row in rows:
        writer.writerow(row)
    
    return response


def dip(request, app_vars=settings.DEFAULT_APP_NAME):
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object

    if app_vars in settings.DIP_CONFIGURED:
        dipData1 = getDipVals(app_vars)
    else:
        dipData1 = []

    if app_vars in settings.DIP_STRUCT_CONFIGURED:
        dipData2 = getStructDipVals(app_vars)
    else:
        dipData2 = []

    if app_vars in settings.SUB_GROUPS:
        dipGroups = settings.SUB_GROUPS[app_vars]
    else:
        dipGroups = []

    if app_vars in settings.SUB_GROUPS_STRUCT:
        dipStructs = settings.SUB_GROUPS_STRUCT[app_vars]
    else:
        dipStructs = []
    
    if not dipData1 and not dipData2:
        dipData = []
    elif not dipData1:
        dipData = dipData2
    elif not dipData2:
       dipData = dipData1
    else:
        dipData = {**dipData1, **dipData2}

    context = {
        'app_vars': app_vars,
        'dipData': dipData,
        'dipGroups': dipGroups,
        'dipStructs': dipStructs
    }
    return render(request, 'epdtdi_timber/plots.html', context)

def dipVals(request, app_vars=settings.DEFAULT_APP_NAME):
    authorised = userAuthorised(request, app_vars)
    if authorised != True:
        return authorised # will return HttpResponseRedirect object

    if app_vars in settings.DIP_CONFIGURED:
        data = getDipVals(app_vars)
    else:
        data = []

    if app_vars in settings.DIP_STRUCT_CONFIGURED:
        data2 = getStructDipVals(app_vars)
    else:
        data2 = []

    if not data and not data2:
        json_data = json.dumps([])
    elif not data:
        json_data = json.dumps(data2)
    elif not data2:
        json_data = json.dumps(data)
    else:
        json_data = json.dumps({**data, **data2})

    return HttpResponse(json_data, content_type="application/json")